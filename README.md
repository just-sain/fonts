# my must have fonts :3

I often reinstall my OS, everytime I need to remember and search fonts for base configuration my neovim, vscode, zsh, wezterm and etc.

## clone

to clone use:

git clone 

## windows 

just highlight ttf/woff/woff2 font files and open context menu, there will be:

```
Install ⬇️
--- --- --- --- --- ---
Install for all users 🔒
```

## linux

in linux you need to:

1. #### go to _folder_ ``` fonts ```

   use **cd** for it

2. #### remove ``` .git ``` _folder_ and ``` README.md ``` _file_
   
   removing **.git**
   
   ```
   rm -rf ./.git
   ```

3. #### remove ``` README.md ```

   removing **README.md**

   ```
   rm ./README.md
   ```

4. #### now we can move all folders with fonts in to the ``` /usr/share/fonts ```

   move all fonts to ``` /usr/share/fonts ```

   ```
   sudo cp -R ~/Downloads/fonts /usr/share/
   ```


thats all :p
